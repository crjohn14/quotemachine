

$(document).ready(function() {
  
  // Twitter developer functions
  window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));
  
  // GET JSON data from quotesondesign
  $data = $.getJSON("http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=30&callback=", function(a) {
      console.log($data);
      return a;
    });
  
  // Get Quote button writes a random quote to the page
  $("#getMessage").on("click", function() {
    var dataArr = $data.responseJSON;
      
    var ranNum = Math.floor(Math.random() * dataArr.length);
      
    $("#quote-text").html(dataArr[ranNum].content);
      
    $("#author-text").html("-- " + dataArr[ranNum].title);
  });
  
  // Tweet button opens a window to Twitter with the quote as input
  $("a.tweet").click(function(e) {
      
    e.preventDefault();
      
    var quote = $("#quote-text").text();
      
    var author = $("#author-text").text();
      
    window.open('https://twitter.com/intent/tweet?text=' + quote + "  -" + author + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + ($(window).width() / 2 - 275) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
  });
});

