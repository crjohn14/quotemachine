# README #

Quote Machine

Author: Chris Johnson
freeCodeCamp.com 
Intermediate front end developer project

This project utilizes quotesondesign API (https://quotesondesign.com/api-v4-0/) to generate random quotes, 
and Twitter's API (https://dev.twitter.com/overview/api) to tweet the random quote.
